# Maintainer: Luca Weiss <luca@z3ntu.xyz>
_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor
pkgver=5.12.13
pkgrel=0
_commit="8609678aeea7a4460cdc7e9035298c4832b5f7e1"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-nftables"
makedepends="
	bison
	findutils
	flex
	gmp-dev
	installkernel
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	"
source="https://gitlab.com/postmarketOS/linux-postmarketos/-/archive/$_commit/linux-postmarketos-$_commit.tar.gz
	config-$_flavor.armv7
	"
builddir="$srcdir/linux-postmarketos-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
68367b794603243b64bbb34ecd0833b40379d14c8c7a949a024e98ab3ae22c9a00619813aaa8dbf0addc902dd87ed9282b42ff425b80cff85be1bb1bf66eca6f  linux-postmarketos-8609678aeea7a4460cdc7e9035298c4832b5f7e1.tar.gz
b9606875f4d78531ca0b9af739e0efdb0e68b54d22b24ba64b2e8f0c54f91aa996222395296ccf7744f8e29067610b83de5a9ba6f92df51b07a26aa2f1c800ff  config-postmarketos-qcom-msm8974.armv7
"
